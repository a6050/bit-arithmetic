package ru.babaninnv.otus.bitarithmetic.calculators;

import ru.babaninnv.otus.bitarithmetic.Bitboard;

public class RookBitMask {

    public long calculate(int input) {

        final long startPosition = 1L << input;
        long bitMask = 0L;

        for (long position = startPosition << 8; position != 0; position <<= 8) {
            bitMask |= position;
        }

        for (long position = startPosition << 1; (position & Bitboard.COLUMN_A_INVERTED) != 0; position <<= 1) {
            bitMask |= position;
        }

        for (long position = startPosition >>> 8 ; position != 0; position >>>= 8) {
            bitMask |= position;
        }

        for (long position = startPosition >>> 1; (position & Bitboard.COLUMN_H_INVERTED) > 0; position >>>= 1) {
            bitMask |= position;
        }

        return bitMask;
    }
}
