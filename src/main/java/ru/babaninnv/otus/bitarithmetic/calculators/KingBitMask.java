package ru.babaninnv.otus.bitarithmetic.calculators;

import ru.babaninnv.otus.bitarithmetic.Bitboard;

public class KingBitMask {

	public long calculate(int value) {
		long position = 1L << value;

		long leftPos = position & Bitboard.COLUMN_A_INVERTED;
		long rightPos = position & Bitboard.COLUMN_H_INVERTED;

		long bitMask = 0;
		bitMask |= leftPos << 7;
		bitMask |= position << 8;
		bitMask |= rightPos << 9;
		bitMask |= leftPos >>> 1;
		bitMask |= rightPos << 1;
		bitMask |= leftPos >>> 9;
		bitMask |= position >>> 8;
		bitMask |= rightPos >>> 7;

		return bitMask;
	}
}
