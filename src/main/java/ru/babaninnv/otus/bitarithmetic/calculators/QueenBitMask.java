package ru.babaninnv.otus.bitarithmetic.calculators;

import java.math.BigInteger;

public class QueenBitMask {

    private final RookBitMask rookBitMask;
    private final BishopBitMask bishopBitMask;

    public QueenBitMask(RookBitMask rookBitMask, BishopBitMask bishopBitMask) {
        this.rookBitMask = rookBitMask;
        this.bishopBitMask = bishopBitMask;
    }

    public long calculate(int input) {
        return BigInteger.valueOf(rookBitMask.calculate(input)).or(BigInteger.valueOf(bishopBitMask.calculate(input)))
                .longValue();
    }
}
