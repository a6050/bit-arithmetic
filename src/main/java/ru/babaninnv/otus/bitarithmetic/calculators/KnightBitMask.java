package ru.babaninnv.otus.bitarithmetic.calculators;

import ru.babaninnv.otus.bitarithmetic.Bitboard;

public class KnightBitMask {
    public long calculate(int input) {
        long position = 1L << input;
        long leftLimitPosition = position & Bitboard.COLUMN_A_INVERTED;
        long doubleLeftLimitPosition = position & (Bitboard.COLUMN_A_INVERTED & Bitboard.COLUMN_B_INVERTED);
        long rightLimitPosition = position & Bitboard.COLUMN_H_INVERTED;
        long doubleRightLimitPosition = position & (Bitboard.COLUMN_G_INVERTED & Bitboard.COLUMN_H_INVERTED);

        return leftLimitPosition << 15
                | rightLimitPosition << 17
                | doubleRightLimitPosition << 10
                | doubleRightLimitPosition >>> 6
                | rightLimitPosition >>> 15
                | leftLimitPosition >>> 17
                | doubleLeftLimitPosition >>> 10
                | doubleLeftLimitPosition << 6;
    }
}
