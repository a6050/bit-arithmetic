package ru.babaninnv.otus.bitarithmetic.calculators;

import ru.babaninnv.otus.bitarithmetic.Bitboard;

public class BishopBitMask {

	public long calculate(int input) {
		final long startPosition = 1L << input;
		long mask = 0L;

		for (long position = startPosition << 9; (position & Bitboard.COLUMN_A_INVERTED) > 0 || position == Long.MIN_VALUE; position <<= 9) {
			mask |= position;
		}

		for (long position = startPosition >>> 7; (position & Bitboard.COLUMN_A_INVERTED) > 0; position >>>= 7) {
			mask |= position;
		}

		for (long position = startPosition >>> 9 ; (position & Bitboard.COLUMN_H_INVERTED) > 0; position >>>= 9) {
			mask |= position;
		}

		for (long position = startPosition << 7; (position & Bitboard.COLUMN_H_INVERTED) > 0; position <<= 7) {
			mask |= position;
		}

		return mask;
	}
}
