package ru.babaninnv.otus.bitarithmetic;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

public class MovesCounter {

    private final Map<Long, Integer> population = new HashMap<>();

    public MovesCounter() {
        population.put(0L, 0);
        population.put(1L, 1);
        population.put(2L, 1);
        population.put(3L, 2);
        population.put(4L, 1);
        population.put(5L, 2);
        population.put(6L, 2);
        population.put(7L, 3);
        population.put(8L, 1);
        population.put(9L, 2);
        population.put(10L, 2);
        population.put(11L, 3);
        population.put(12L, 2);
        population.put(13L, 3);
        population.put(14L, 3);
        population.put(15L, 4);
    }

    public int count(long maskAsLong) {
        int count = 0;
        BigInteger mask = new BigInteger(Long.toUnsignedString(maskAsLong));
        while (mask.compareTo(BigInteger.ZERO) > 0) {
            BigInteger shift = mask.and(BigInteger.valueOf(15L));
            count += population.get(shift.longValue());
            mask = mask.shiftRight(4);
        }

        return count;
    }
}
