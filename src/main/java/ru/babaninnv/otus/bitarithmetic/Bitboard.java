package ru.babaninnv.otus.bitarithmetic;

public class Bitboard {
	public final static long COLUMN_A_INVERTED = 0xfefefefefefefefeL;
	public final static long COLUMN_B_INVERTED = 0xfdfdfdfdfdfdfdfdL;
	public final static long COLUMN_G_INVERTED = 0xbfbfbfbfbfbfbfbfL;
	public final static long COLUMN_H_INVERTED = 0x7f7f7f7f7f7f7f7fL;
}
