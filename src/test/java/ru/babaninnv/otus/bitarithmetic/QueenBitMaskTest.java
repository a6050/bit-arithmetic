package ru.babaninnv.otus.bitarithmetic;

import org.junit.Test;
import ru.babaninnv.otus.bitarithmetic.calculators.BishopBitMask;
import ru.babaninnv.otus.bitarithmetic.calculators.QueenBitMask;
import ru.babaninnv.otus.bitarithmetic.calculators.RookBitMask;
import ru.babaninnv.otus.bitarithmetic.test.Task;
import ru.babaninnv.otus.bitarithmetic.test.Tester;

import java.io.File;

public class QueenBitMaskTest {

    @Test
    public void calculate() {
        new Tester(new BitMaskTask(), new File("src/test/resources/queen").getAbsolutePath()).runTests();
    }

    public static class BitMaskTask implements Task {

        public final MovesCounter movesCounter = new MovesCounter();

        @Override
        public String[] run(String[] data) {
            int value = Integer.parseInt(data[0]);
            QueenBitMask bitMask = new QueenBitMask(new RookBitMask(), new BishopBitMask());
            long result = bitMask.calculate(value);
            return new String[] {
                    Long.toUnsignedString(movesCounter.count(result)),
                    Long.toUnsignedString(result)
            };
        }
    }
}