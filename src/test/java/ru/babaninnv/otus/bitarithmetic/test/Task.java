package ru.babaninnv.otus.bitarithmetic.test;

public interface Task {
    String[] run(String[] data);
}