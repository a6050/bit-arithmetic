package ru.babaninnv.otus.bitarithmetic.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Tester {
    private final Task task;
    private final String path;

    public Tester(Task task, String path) {
        this.task = task;
        this.path = path;
    }

    public void runTests() {
        int nr = 1;
        while (true) {
            File inFile = new File(String.format("%s/test.%d.in", path, nr));
            File outFile = new File(String.format("%s/test.%d.out", path, nr));

            if (!inFile.exists() || !outFile.exists()) {
                break;
            }

            TestResult testResult = runTest(inFile, outFile);

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Test: ").append(nr)
                    .append(", result: ").append(testResult.result ? "Успех" : "Неуспешно");
            if (testResult.result) {
                stringBuilder.append(", time: ").append(testResult.time).append(" ns");
            }
            System.out.println(stringBuilder);
            nr++;
        }
    }

    public TestResult runTest(File inFile, File outFile) {
        try {
            String[] data = readAllLines(inFile);
            String[] expect = readAllLines(outFile);

            long start = System.nanoTime();
            String[] actual = task.run(data);
            long time = System.nanoTime() - start;

            return new TestResult(actual != null && Arrays.equals(actual, expect), time);
        } catch (Exception e) {
            e.printStackTrace();
            return new TestResult(false, null);
        }
    }

    private static String[] readAllLines(File file) {
        List<String> result = new ArrayList<>();

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.add(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("Ошибка при чтении файла: " + file.getAbsolutePath(), e);
        }

        return result.toArray(new String[0]);
    }

    private static String readAllText(File file) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("Ошибка при чтении файла: " + file.getAbsolutePath(), e);
        }

        return stringBuilder.toString();
    }
}
