package ru.babaninnv.otus.bitarithmetic.test;

public class TestResult {
    public boolean result;
    public Long time;

    public TestResult(boolean result, Long time) {
        this.result = result;
        this.time = time;
    }
}
